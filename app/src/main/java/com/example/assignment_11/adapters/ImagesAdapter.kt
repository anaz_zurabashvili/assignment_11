package com.example.assignment_11.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment_11.databinding.LayoutImageBinding
import com.example.assignment_11.models.Image

typealias OnImageClick = (position: Int) -> Unit

class ImagesAdapter :
    RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    private val images = mutableListOf<Image>()
    var onImageClick: OnImageClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: ImagesAdapter.ViewHolder,
        position: Int
    ) =
        holder.onBind()


    inner class ViewHolder(private val binding: LayoutImageBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private lateinit var model: Image

        fun onBind() {
            model = images[adapterPosition]
            binding.ivImage.setImageResource(model.image)
            binding.root.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            onImageClick?.invoke(adapterPosition)

        }
    }


    override fun getItemCount() = images.size


    fun setData(images: MutableList<Image>) {
        this.images.clear()
        this.images.addAll(images)
        notifyDataSetChanged()
    }

}
