package com.example.assignment_11.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.assignment_11.databinding.ActivityImageBinding
import com.example.assignment_11.models.Image

class ImageActivity : AppCompatActivity() {
    private lateinit var binding: ActivityImageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }
    private fun init(){
        binding.btnBack.setOnClickListener(btnBackClick)
        if (intent.extras?.containsKey("image") == true) {
            getInfo(intent.extras?.get("image") as Image)
        }
    }

    fun getInfo(image: Image) {
        binding.ivImage.setImageResource(image.image)
        binding.tvTitle.text = image.title
        binding.tvContent.text = image.content
    }

    private val btnBackClick = View.OnClickListener {
        val intent = intent
        setResult(RESULT_OK, intent)
        finish()

    }
}