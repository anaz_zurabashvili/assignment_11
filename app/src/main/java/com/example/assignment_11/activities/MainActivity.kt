package com.example.assignment_11.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import com.example.assignment_11.R
import com.example.assignment_11.adapters.ImagesAdapter
import com.example.assignment_11.databinding.ActivityMainBinding
import com.example.assignment_11.models.Image

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val images = mutableListOf<Image>()
    private var filteredImages = mutableListOf<Image>()
    private lateinit var adapter: ImagesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        setData()
        initRecyclerView()
        binding.etSearch.setOnEditorActionListener { textView, actionId, keyEvent ->
            searchImages(textView)
            true
        }
    }

    private fun filterImages(search: String) =
        images.filter { image ->
            image.title.trim().lowercase().contains(search.trim().lowercase())
        } as MutableList<Image>

    private fun searchImages(textView: TextView) {
        if (!textView.text.toString().isNullOrBlank()) {
            val data = filterImages(textView.text.toString())
            if (data.size > 0) {
                binding.tvAllResults.text =
                    getString(R.string.all_results) + " ${textView.text}"
                adapter.setData(data)
                filteredImages = data
            } else {
                binding.tvAllResults.text =
                    getString(R.string.nothing_found) + " ${textView.text}"
                adapter.setData(data)
                filteredImages = data
            }
        } else {
            binding.tvAllResults.text = getString(R.string.all_results)
            adapter.setData(images)
            filteredImages = images
        }
    }

    private fun initRecyclerView() {
        binding.recyclerView.layoutManager = GridLayoutManager(this, 3)
        adapter = ImagesAdapter()
        binding.recyclerView.adapter = adapter
        adapter.setData(filteredImages)
        adapter.onImageClick = {
            onImageClick(it)
        }
    }

    private fun onImageClick(position: Int) {
        val intent = Intent(this, ImageActivity::class.java)
        intent.putExtra("image", filteredImages[position])
        startActivity(intent)
    }


    private fun setData() {
        images.add(Image(R.mipmap.im_1, "Dog 1", "Content 1"))
        images.add(Image(R.mipmap.im_3, "Cat 2", "Content 2"))
        images.add(Image(R.mipmap.im_1, "Dog 3", "Content 3"))
        images.add(Image(R.mipmap.im_2, "Dog 4", "Content 4"))
        images.add(Image(R.mipmap.im_3, "Cat 5", "Content 5"))
        images.add(Image(R.mipmap.im_3, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_1, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_1, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_4, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_1, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_4, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_4, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_1, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_4, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_4, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_1, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_4, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_4, "Cat 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Dog 6", "Content 6"))
        images.add(Image(R.mipmap.im_2, "Cat 6", "Content 6"))
        filteredImages = images
    }

}