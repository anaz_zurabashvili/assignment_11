package com.example.assignment_11.activities

import android.content.Intent


import android.os.Bundle
import android.os.Handler

import androidx.appcompat.app.AppCompatActivity
import com.example.assignment_11.databinding.ActivitySplashScreenBinding


class SplashScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Handler().postDelayed(
            {
                startActivity(Intent(this@SplashScreenActivity, MainActivity::class.java))
                finish()
            }, 3000
        )

    }

}