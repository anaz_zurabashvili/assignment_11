package com.example.assignment_11.models

import android.os.Parcelable
import com.example.assignment_11.R
import kotlinx.parcelize.Parcelize


typealias MIPMAPS = R.mipmap

@Parcelize
data class Image(var image: Int, var title: String, var content: String) : Parcelable